import numpy as np
from cec2017.functions import f4


def tournament_reproduction(population, evaluations, population_size):
    new_population = []
    for i in range(population_size):
        x1, x2 = np.random.randint(population_size, size=2)
        new_population.append(
            population[x1] if evaluations[x1] < evaluations[x2] else population[x2])
    return new_population


def elitary_succession(last_population, current_population, last_evaluations, current_evaluations, elite_size):
    return zip(*(sorted(list(zip(last_evaluations, last_population)))[:elite_size] + sorted(list(zip(current_evaluations, current_population)), reverse=True)[elite_size:]))


def find_best(population, evaluations):
    return min(zip(evaluations, population))


def mutate(population, mutation_strength):
    return [i + np.random.uniform(-mutation_strength, mutation_strength, 10) for i in population]


def evaluate(population, eval_function):
    return [eval_function(i) for i in population]


def evolution(eval_function, population, population_size, mutation_strength, number_of_iterations, elite_size):
    current_iteration = 0
    evaluations = evaluate(population, eval_function)
    best_evaluation, best_individual = find_best(population, evaluations)

    while current_iteration < number_of_iterations:
        population_R = tournament_reproduction(
            population, evaluations, population_size)
        population_M = mutate(population_R, mutation_strength)
        current_evaluations = evaluate(population_M, eval_function)
        it_best_evaluation, it_best_individual = find_best(
            population_M, current_evaluations)
        if it_best_evaluation <= best_evaluation:
            best_evaluation = it_best_evaluation
            best_individual = it_best_individual
        evaluations, population = elitary_succession(
            population, population_M, evaluations, current_evaluations, elite_size)
        current_iteration += 1
    return best_evaluation


with open("res.csv", "w+") as f:
    for population_size in [10, 20, 50, 100, 200, 500]:
        number_of_iterations = 10000 // population_size - 1

        for elite_size in [1, 3, 5]:
            for mutation_strength in [0.1, 1, 10]:
                for i in range(30):
                    f.write(
                        f'{population_size},{elite_size},{mutation_strength},{evolution(f4, [np.random.uniform(low=-100, high=100, size=10) for i in range(population_size)], population_size, mutation_strength, number_of_iterations, elite_size)}\n')
