# Klasyczny algorytm ewolucyjny


## Zadanie

Zaimplementować klasyczny algorytm ewolucyjny bez krzyżowania z selekcją turniejową i sukcesją elitarną. Dostępny budżet to 10000 ewaluacji funkcji celu. Optymalizujemy funkcję numer 4 z CEC 2017.

## Wnioski

### Siła mutacji
Dla wysokiej siły mutacji średnia wartość i odchylenie standardowe są lepsze,
gorzej wypada wartość minimalna - eksploracja kosztem eksploatacji.
Najgorze wyniki daje bardzo mała siła mutacji.

### Rozmiar populacji
Do znajdowania dokładnych wartości minimum najlepsze są małe populacje,
niższą średnią i odchylenie standardowe daje duża populacja. 

### Rozmiar elity
Najlepiej wypada większa elita o rozmiarze 5,
ponieważ dla większych elit algorytm szybciej zbiega do minimum lokalnego,
co jest pozytywne dla podanej funkcji.
